
# coding: utf-8

# In[7]:

from random import randrange #permettra d'obtenir des valeurs aléatoires.
from copy import deepcopy #permettra de copier des grilles en évitant les problèmes.
from tkinter import * #importation de la bibliothèque tkinter pour l'interface graphique.
import time #permet la gestion du temps

xg = 30 #largeur de la grille
yg = 30 #hauteur de la grille
tour = 0 #nombres de tours effectués
tour_fin = 0 #nombres de tours maximum
bordure = 1 #présence de bordure sur la grille
identique = 0 #indique si les grilles sont identiques ou non
cv = 0 #nombre de cellules vivantes dans la grille

g=[] #matrice
h=[] #permet de remplir les lignes de la matrice.

#FONCTIONS DE GENERATION

def g_vide(): #permet de vider la grille
    can.delete(ALL) #permet de supprimer tous les objects du canevas.
    global g, xg, yg, tour, identique, cv #permet d'utiliser la grille, la largeur et la hauteur en tant que variable globales.
    identique = 0 #la nouvelle grille n'est plus identique à la précédente
    tour = 0 #le nombre de tours est réinitialisé
    cv = 0 #le compteur de cellules vivantes est réinitialisé
    text_cellulesvivantes2.config(text=cv) #le texte affichant le nombre de cellules vivantes est actualisé
    text_compteur.config(text=tour) #le texte affichant le compteur de tours est actualisé
    g_n=[] #création d'une nouvelle grille
    for i in range(0, yg): #pour chaque ligne
        h=[] #création d'une nouvelle liste
        for j in range(0, xg): #pour chaque case
            can.create_rectangle((j*10),(i*10),(j*10)+10,(i*10)+10, fill='white') #création de cases blanches
            h.append(0) #ajout de la valeur correspondante à la case dans h
        g_n.append(h) #ajout de la liste h correspondant à une ligne de la grille, formant une grille
    g =deepcopy(g_n) #g prend la valeur de la nouvelle grille, avec deepcopy pour prendre en compte l'imbriquement des listes
    
def g_alea(): #permet de generer une grille aléatoirement.
    global g,xg,yg,tour, identique #représentent la grille, sa largeur et sa hauteur.
    identique = 0 #la nouvelle grille n'est plus identique à la précédente
    tour = 0 #le nombre de tours est réinitialisé
    cv = 0 #le compteur de cellules vivantes est réinitialisé
    text_compteur.config(text=tour)
    g_n=[] #créé une nouvelle grille sous forme de liste de listes.
    for i in range(0, yg): #pour chaque ligne
        h=[] #crée une nouvelle liste représentant les cases de chaque ligne de la grille.
        for j in range(0, xg): #pour chaque case
            hasard = randrange(0,2) #affecte une valeur à chaque case comprise entre 0 (blanche) et 1 (noire).
            if hasard == 1: #si la case est noire
                can.create_rectangle((j*10),(i*10),(j*10)+10,(i*10)+10, fill='black') #crée une case noire sur le canevas.
                cv = cv + 1
            else: #si la case est blanche
                can.create_rectangle((j*10),(i*10),(j*10)+10,(i*10)+10, fill='white') #crée une case blanche sur le canevas.
            h.append(hasard) #ajoute la valeur à la liste h.
        g_n.append(h) #ajoute la liste h à la liste g pour former une grille.
    g =deepcopy(g_n) #permet à g de prendre la valeur de g_n en prennant en compte qu'il y a des listes dans la liste.
    text_cellulesvivantes2.config(text=cv)
    
def g_alea_sym_v(): #permet de generer une grille aléatoirement avec symétrie verticale.
    global g, xg, yg, tour, identique, cv #représentent la grille, sa largeur et sa hauteur.
    identique = 0 #la nouvelle grille n'est plus identique à la précédente
    tour = 0 #le nombre de tours est réinitialisé
    cv = 0 #le compteur de cellules vivantes est réinitialisé
    text_compteur.config(text=tour) #le texte affichant le compteur de tours est actualisé
    if xg%2 == 1: #si la largeur est impaire (présence d'une colonne centrale)
        g_n=[] #création d'une nouvelle liste
        for i in range(0, yg): #pour chaque ligne
            h=[] #création d'une liste h, représentant la première moitié de la ligne (médiane inclue)
            h2=[] #création d'une liste h, représentant la deuxième partie de la ligne (médiane exclue)
            for j in range(0, xg//2+1): #pour chaque case, jusqu'à la case médiane
                hasard = randrange(0,2) #affecte une valeur à chaque case comprise entre 0 (blanche) et 1 (noire).
                if hasard == 1: #si la case est noire.
                    can.create_rectangle((j*10),(i*10),(j*10)+10,(i*10)+10, fill='black') #crée une case noire sur le canevas.
                    can.create_rectangle(((xg-j-1)*10),(i*10),((xg-j-1)*10)+10,(i*10)+10, fill='black') #crée une case noire symétrique.
                    if j == xg//2: #si la cellule se situe sur la ligne du milleu
                        cv = cv + 1 #les cases n'ont pas de symétriques, donc on ajoute qu'une seule cellule au compteur
                    else: #sinon
                        cv = cv + 2 #le compteur de cellule vivante augmente de deux
                else: #si la case est blanche.
                    can.create_rectangle((j*10),(i*10),(j*10)+10,(i*10)+10, fill='white') #crée une case blanche sur le canevas.
                    can.create_rectangle(((xg-j-1)*10),(i*10),((xg-j-1)*10)+10,(i*10)+10, fill='white') #crée une case blanche symétrique.
                h.append(hasard) #ajoute la valeur de la case à la liste h
                if j != xg//2: #si j n'est pas au milieu de la liste
                    h2.append(hasard) #on ajoute également la valeur de la case à la liste h2
            h2.reverse() #on retourne la liste h2 pour la symétrie
            h = h + h2 #on fusionne les deux listes pour obtenir une ligne complète
            g_n.append(h) #on ajoute la liste de la ligne à g_n pour obtenir une grille
        g=deepcopy(g_n) #g prend la valeur de g_n, en prennant on compte la hiérarchie des listes dans g_n
    else: #sinon la largeur est paire (pas de colonne centrale)
        g_n=[] #création d'une nouvelle liste
        for i in range(0, yg): #pour chaque ligne
            h=[] #création d'une liste h, représentant la première moitié de la ligne
            h2=[] #création d'une liste h, représentant la deuxième moitié de la ligne
            for j in range(0, xg//2): #pour chaque case, jusqu'à la moitié de la ligne
                hasard = randrange(0,2) #attribue une valeur comprise entre 0 (blanc) et 1 (noir) à chaque case)
                if hasard == 1: #si la case est noire
                    can.create_rectangle((j*10),(i*10),(j*10)+10,(i*10)+10, fill='black') #crée une case noire sur le canevas.
                    can.create_rectangle(((xg-j-1)*10),(i*10),((xg-j-1)*10)+10,(i*10)+10, fill='black') #crée une case noire symétrique.
                    cv = cv + 2
                else: #sinon la case est blanche
                    can.create_rectangle((j*10),(i*10),(j*10)+10,(i*10)+10, fill='white') #crée une case blanche sur le canevas.
                    can.create_rectangle(((xg-j-1)*10),(i*10),((xg-j-1)*10)+10,(i*10)+10, fill='white') #crée une case blanche symétrique.
                h.append(hasard) #ajout de la valeur de la case dans la liste h
                h2.append(hasard) #ajout de la valeur de la case dans la liste h2
            h2.reverse() #on retourne h2 pour qu'elle soit symétrique par rapport à h
            h = h + h2 #on fusionne les listes pour former une liste représentant la ligne entière
            g_n.append(h) #on ajoute la liste h à g_n pour former une grille
        g=deepcopy(g_n) #g prend la valeur de g_n, en prennant on compte la hiérarchie des listes dans g_n
    text_cellulesvivantes2.config(text=cv)
        
def g_alea_sym_h(): #permet de générer une grille avec symétrie horizontale.
    global g, xg, yg, tour, identique, cv
    identique = 0 #la nouvelle grille n'est plus identique à la précédente
    tour = 0 #le nombre de tours est réinitialisé
    cv = 0 #le compteur de cellules vivantes est réinitialisé
    text_compteur.config(text=tour) #le texte affichant le compteur de tour est actualisé
    if yg%2 == 1: #si le nombre de ligne est impair (présence d'une ligne centrale
        g_n=[] #création d'une nouvelle grille
        for i in range(0, yg//2+1): #pour chaque ligne, jusqu'à la ligne centrale
            h=[] #création d'une liste h, représentant la ligne
            for j in range(0, xg): #pour chaque case de la ligne
                hasard = randrange(0,2) #attribution d'une valeur aléaoire 0 (blanc) ou 1 (noir)
                if hasard == 1: #si la case est noire
                    can.create_rectangle((j*10),(i*10),(j*10)+10,(i*10)+10, fill='black') #crée une case noire symétrique.
                    can.create_rectangle((j*10),((yg-i-1)*10),(j*10)+10,((yg-i-1)*10)+10, fill='black') #crée une case noire sur le canevas.
                    if i == yg//2: #si i est sur la ligne du milieu
                        cv = cv + 1 #la case est dans la ligne du milieu, donc pas de cases symétriques, on n'augmente que de 1
                    else: #sinon, i correspond à toutes les autres lignes
                        cv = cv + 2 #on ajoute donc 2 au compteur de cellules vivantes
                else: #sinon elle est blanche
                    can.create_rectangle((j*10),(i*10),(j*10)+10,(i*10)+10, fill='white') #crée une case blanche sur le canevas.
                    can.create_rectangle((j*10),((yg-i-1)*10),(j*10)+10,((yg-i-1)*10)+10, fill='white') #crée une case blanche sur le canevas.
                h.append(hasard) #ajoute la valeur de la nouvelle case à la liste
            g_n.append(h) #ajoute la liste à la nouvelle grille
        g_n2=deepcopy(g_n) #g_n2, la grille complémentaire de g_n prend sa valeur
        g_n2.remove(g_n2[yg//2]) #on retire la ligne centrale de la grille complémentaire
        g_n2.reverse() #on inverse la grille g_n2 pour la symétrie
        g_n = g_n + g_n2 #on fusionne les deux grilles pour obtenir une grille symétrique représentant la grille entière
        g=deepcopy(g_n) #g prend la valeur de la nouvelle grille, avec prise en compte de l'imbriquement
    else: #sinon le nombre de ligne est pair (pas de ligne centrale
        g_n=[] #création d'une nouvelle liste
        for i in range(0, yg//2): #pour chaque ligne, jusqu'à la moitié
            h=[] #création d'une liste h, représentant une ligne
            for j in range(0, xg): #pour chaque case de la ligne
                hasard = randrange(0,2) #attribuer une valeur aléatoire à chaque case 0 (blanc) et 1 (noir)
                if hasard == 1: #si la case est noire
                    can.create_rectangle((j*10),(i*10),(j*10)+10,(i*10)+10, fill='black') #crée une case noire symétrique.
                    can.create_rectangle((j*10),((yg-i-1)*10),(j*10)+10,((yg-i-1)*10)+10, fill='black') #crée une case noire sur le canevas.
                    cv = cv + 2
                else: #sinon elle est blanche
                    can.create_rectangle((j*10),(i*10),(j*10)+10,(i*10)+10, fill='white') #crée une case blanche sur le canevas.
                    can.create_rectangle((j*10),((yg-i-1)*10),(j*10)+10,((yg-i-1)*10)+10, fill='white') #crée une case blanche sur le canevas.
                h.append(hasard) #on ajoute la valeur de la case à la liste h
            g_n.append(h) #on ajoute la liste de la ligne à la nouvelle grille
        g_n2=deepcopy(g_n) #la grille complémentaire de g_n prend sa valeur
        g_n2.reverse() #on l'inverse pour la symétrie
        g_n = g_n + g_n2 #on fusionne les deux listes pour recouvrir toutes les cases
        g=deepcopy(g_n) #g prend la valeur de g_n avec prise en compte de l'imbriquement des listes
    text_cellulesvivantes2.config(text=cv)
    
def g_planeur():
    global g, xg, yg, tour, identique, cv
    if xg < 5 or yg < 5:
        erreur4()
    else:
        identique = 0
        tour = 0
        cv = 5
        text_compteur.config(text=tour)
        text_cellulesvivantes2.config(text=cv)
        can.delete(ALL)
        g_n=[]
        
        #LIGNES AVANT LE PLANEUR
        for i in range(0, yg//2-1):
            h=[]
            for j in range(0, xg):
                can.create_rectangle((j*10),(i*10),(j*10)+10,(i*10)+10, fill='white')
                h.append(0)
            g_n.append(h)
        
        #PREMIERE LIGNE DU PLANEUR
        h=[]
        for j in range(0, xg//2-1):
            h.append(0)
        h.append(1)
        h.append(1)
        h.append(1)
        for j in range(xg//2+2, xg):
            h.append(0)
        g_n.append(h)
        
        #DEUXIEME LIGNE DU PLANEUR
        h=[]
        for j in range(0, xg//2+1):
            h.append(0)
        h.append(1)
        for j in range(xg//2+2, xg):
            h.append(0)
        g_n.append(h)
        
        #TROISIEME LIGNE DU PLANEUR
        h=[]
        for j in range(0, xg//2):
            h.append(0)
        h.append(1)
        for j in range(xg//2+1, xg):
            h.append(0)
        g_n.append(h)
        
        #LIGNES APRES LE PLANNEUR
        for i in range(yg//2+2, yg):
            h=[]
            for j in range(0, xg):
                h.append(0)
            g_n.append(h)
        g=deepcopy(g_n)
        
        #GENERATION DU PLANEUR
        for i in range(0, yg): #pour chaque ligne de la grille
            for j in range(0, xg): #pour chaque case de chaque ligne
                if g[i][j] == 0: #si la valeur de la case est 0
                    can.create_rectangle((j*10),(i*10),(j*10)+10,(i*10)+10, fill='white') #alors on crée une case blanche
                else: #sinon
                    can.create_rectangle((j*10),(i*10),(j*10)+10,(i*10)+10, fill='black') #on crée une case noire
                    
def g_clignotant():
    global g, xg, yg, tour, identique, cv #grille, largeur, hauteur, nombre de tours effectués, identique et nombre de cellules vivantes sont globales
    if xg < 5 or yg < 5: #si les dimensions de la grille ne sont pas suffisantes
        erreur4() #renvoie vers l'erreur 4
    else: #sinon, on génère le clignotant
        identique = 0 #la nouvelle grille n'est pas ou plus identique à la précédente
        tour = 0 #le compteur de tours est réinitialisé
        cv = 3 #le compteur de cellules vivantes est mis à 3
        text_compteur.config(text=tour) #le texte affichant le nombre de tours est actualisé
        text_cellulesvivantes2.config(text=cv) #le texte affichant le nombre de cellules vivantes est actualisé
        can.delete(ALL) #suppression de tous les éléments du canevas
        g_n=[] #création d'une nouvelle grille
        for i in range(0, yg//2): #pour toutes les lignes avant le clignotant
            h=[] #création d'une liste h représentant une ligne
            for j in range(0, xg): #pour chaque case de la ligne
                h.append(0) #on ajoute une case morte à h
            g_n.append(h) #on ajoute la liste h à la grille g_n
        
        #CLIGNOTANT
        h=[] #création d'une liste h
        for j in range(0, xg//2-1): #pour toutes les cases de la ligne après le clignotant
            h.append(0) #on ajoute une case morte à h
        h.append(1) #on ajoute une case vivante à h
        h.append(1) #on ajoute une case vivante à h
        h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2+2, xg):
            h.append(0) #on ajoute une case morte à h
        g_n.append(h) #on ajoute la liste h à la grille g_n
        for i in range(yg//2+1, yg):
            h=[] #création d'une liste h
            for j in range(0, xg): #pour toutes les cases de la ligne après le clignotant
                h.append(0) #on ajoute une case morte à h
            g_n.append(h) #on ajoute la liste h à la grille g_n
        g=deepcopy(g_n) #g prend la valeur de g_n avec prise en compte de la hiérarchie des listes dans g_n
        
        #GENERATION DU CLIGNOTANT
        for i in range(0, yg): #pour chaque ligne de la grille
            for j in range(0, xg): #pour chaque case de chaque ligne
                if g[i][j] == 0: #si la valeur de la case est 0
                    can.create_rectangle((j*10),(i*10),(j*10)+10,(i*10)+10, fill='white') #alors on crée une case blanche
                else: #sinon
                    can.create_rectangle((j*10),(i*10),(j*10)+10,(i*10)+10, fill='black') #on crée une case noire

def g_pentadecathlon(): #fonction permettant de créer un pentadécathlon (oscillateur de période 15)
    global g, xg, yg, tour, identique, cv #g, xg et yg globales car les fonctions appelées par le menu tkinter ne prennent pas d'arguments
    if xg < 18 or yg < 11: #si les dimensions sont insuffisantes
        erreur5() #renvoie vers l'erreur 5
    else: #sinon création du pentadécathlon
        identique = 0
        tour = 0
        cv = 10
        text_compteur.config(text=tour)
        text_cellulesvivantes2.config(text=cv)
        can.delete(ALL) #suppression de tous les éléments du canevas
        g_n=[] #création d'une nouvelle grille
        
        #LIGNES AVANT LE PENTADECATHLON
        for i in range(0, yg//2): #de la première ligne à la dernière ligne oùle pentadécathlon n'est pas généré (centré)
            h=[] #création de la liste h, représentant la ligne
            for j in range(0, xg): #pour chaque case de la ligne
                h.append(0) #on ajoute 0 pour chaque case blanche à la liste h
            g_n.append(h) #on ajoutre la liste h à la nouvelle grille
        
        #PENTADECATHLON
        h=[] #création d'une liste pour la ligne où le pentadécathlon est généré
        for j in range(0, xg//2-5): #pour chaque case blanche de la ligne, avant le pentadécathlon
            h.append(0) #ajout de la valeur de la case à la liste h
        for j in range(xg//2-5, xg//2+5): #pour les 10 cases générant le pentadécathlon (centré)
            h.append(1) #ajout de la valeur de la case à la liste h
        for j in range(xg//2+5, xg): #pour chaque case blanche de la ligne, après le pentadécathlon
            h.append(0) #ajout de la valeur de la case à la liste h
        g_n.append(h) #ajout de la liste h à la nouvelle grille g_n
        for i in range(yg//2+1, yg): #pour toutes les lignes après le pentadécathlon
        
        #LIGNES APRES LE PENTADECATHLON
            h=[] #création d'une liste représentant chacune une ligne
            for j in range(0, xg): #pour chaque case de la ligne
                h.append(0) #ajout de la case blanche à la liste
            g_n.append(h) #ajout de la liste h à la grille g_n
        g=deepcopy(g_n) #g prend la valeur de g_n avec prise en compte de l'imbriquement des listes
        
        #GENERATION DU PENTADECATHLON
        for i in range(0, yg): #pour chaque ligne de la grille
            for j in range(0, xg): #pour chaque case de chaque ligne
                if g[i][j] == 0: #si la valeur de la case est 0
                    can.create_rectangle((j*10),(i*10),(j*10)+10,(i*10)+10, fill='white') #alors on crée une case blanche
                else: #sinon
                    can.create_rectangle((j*10),(i*10),(j*10)+10,(i*10)+10, fill='black') #on crée une case noire
                    

def g_canon_a_planeurs(): #fonction permettant de créer un cannon à planeurs
    global g, xg, yg, tour, identique, cv #g, xg et yg globales car les fonctions appelées par le menu tkinter ne prennent pas d'arguments
    if xg < 38 or yg < 11: #si les dimensions sont insuffisantes
        erreur7() #renvoie vers l'erreur 7
    else: #sinon création du planeur
        identique = 0 #la grille est plus identique à la précédente
        tour = 0 #le compteur de tours est réinitialisé
        cv = 36 #le compteur de cellule vivante est placé à 36
        text_compteur.config(text=tour) #le texte affichant le compteur de tours est actualisé
        text_cellulesvivantes2.config(text=cv) #le texte affichant le compteur de cellules vivantes est actualisé
        can.delete(ALL) #suppression de tous les éléments du canevas
        g_n=[] #création d'une nouvelle grille
        
        #LIGNES AVANT LE CANON A PLANEURS
        for i in range(0, yg//2-4): #de la première ligne à la dernière ligne où le planneur n'est pas généré (centré)
            h=[] #création de la liste h, représentant la ligne
            for j in range(0, xg): #pour chaque case de la ligne
                h.append(0) #on ajoute 0 pour chaque case blanche à la liste h
            g_n.append(h) #on ajoutre la liste h à la nouvelle grille
            
        #PREMIERE LIGNE AVANT LE CANON A PLANEURS
        h=[] #on vide la liste h pour obtenir les valeurs des cases de la nouvelle ligne
        for j in range(0, xg//2+6):
            h.append(0) #on ajoute une case morte à h
        h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2+7, xg):
            h.append(0) #on ajoute une case morte à h
        g_n.append(h) #ajout de la liste h à la nouvelle grille g_n
        
        #DEUXIEME LIGNE DU CANON A PLANEURS
        h=[] #on vide la liste h pour obtenir les valeurs des cases de la nouvelle ligne
        for j in range(0, xg//2+4):
            h.append(0) #on ajoute une case morte à h
        h.append(1) #on ajoute une case vivante à h
        h.append(0) #on ajoute une case morte à h
        h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2+7, xg):
            h.append(0) #on ajoute une case morte à h
        g_n.append(h) #on ajoute la liste h à la nouvelle grille
        
        #TROISIEME LIGNE DU CANON A PLANEURS
        h=[] #on vide la liste h pour obtenir les valeurs des cases la nouvelle ligne
        for j in range(0, xg//2-6):
            h.append(0) #on ajoute une case morte à h
        for j in range(xg//2-6, xg//2-4):
            h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2-4, xg//2+2):
            h.append(0) #on ajoute une case morte à h
        for j in range(xg//2+2, xg//2+4):
            h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2+4, xg//2+16):
            h.append(0) #on ajoute une case morte à h
        for j in range(xg//2+16, xg//2+18):
            h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2+18, xg):
            h.append(0) #on ajoute une case morte à h
        g_n.append(h) #on ajoute la liste h à la nouvelle grille
        
        #QUATRIEME LIGNE DU CANON A PLANEURS
        h=[] #on vide la liste h pour obtenir les valeurs de la nouvelle ligne
        for j in range(0, xg//2-7):
            h.append(0) #on ajoute une case morte à h
        h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2-6, xg//2-3):
            h.append(0) #on ajoute une case morte à h
        h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2-2, xg//2+2):
            h.append(0) #on ajoute une case morte à h
        for j in range(xg//2+2, xg//2+4):
            h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2+4, xg//2+16):
            h.append(0) #on ajoute une case morte à h
        for j in range(xg//2+16, xg//2+18):
            h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2+18, xg): 
            h.append(0) #on ajoute une case morte à h
        g_n.append(h) #on ajoute la liste h à la nouvelle grille
        
        #LIGNE CENTRALE DU CANON A PLANEURS
        h=[] #on vide la liste h pour obtenir les valeurs de la nouvelle ligne
        for j in range(0, xg//2-18):
            h.append(0) #on ajoute une case morte à h
        for j in range(xg//2-18, xg//2-16):
            h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2-16, xg//2-8):
            h.append(0) #on ajoute une case morte à h
        h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2-7, xg//2-2):
            h.append(0) #on ajoute une case morte à h
        h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2-1, xg//2+2):
            h.append(0) #on ajoute une case morte à h
        for j in range(xg//2+2, xg//2+4):
            h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2+4, xg):
            h.append(0) #on ajoute une case morte à h
        g_n.append(h) #on ajoute la liste h à la nouvelle grille
        
        #SIXIEME LIGNE DU CANON A PLANEURS
        h=[] #on vide liste h pour obtenir les valeurs de la nouvelle ligne
        for j in range(0, xg//2-18):
            h.append(0) #on ajoute une case morte à h
        for j in range(xg//2-18, xg//2-16):
            h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2-16, xg//2-8):
            h.append(0) #on ajoute une case morte à h
        h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2-7, xg//2-4):
            h.append(0) #on ajoute une case morte à h
        h.append(1) #on ajoute une case vivante à h
        h.append(0) #on ajoute une case morte à h
        for j in range(xg//2-2, xg//2):
            h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2, xg//2+4):
            h.append(0) #on ajoute une case morte à h
        h.append(1) #on ajoute une case vivante à h
        h.append(0) #on ajoute une case morte à h
        h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2+7, xg):
            h.append(0) #on ajoute une case morte à h
        g_n.append(h) #on ajoute la liste h à la nouvelle grille
        
        #SEPTIEME LIGNE DU CANON A PLANEURS
        h=[] #on vide liste h pour obtenir les valeurs des cases de la nouvelle ligne
        for j in range(0, xg//2-8):
            h.append(0) #on ajoute une case morte à h
        h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2-7, xg//2-2):
            h.append(0) #on ajoute une case morte à h
        h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2-1, xg//2+6):
            h.append(0) #on ajoute une case morte à h
        h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2+7, xg):
            h.append(0) #on ajoute une case morte à h
        g_n.append(h) #on ajoute la liste h à la nouvelle grille
        
        #AVANT DERNIERE LIGNE DU CANON A PLANEURS
        h=[] #on vide liste h pour obtenir les valeurs des cases de la nouvelle ligne
        for j in range(0, xg//2-7):
            h.append(0) #on ajoute une case morte à h
        h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2-6, xg//2-3):
            h.append(0) #on ajoute une case morte à h
        h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2-2, xg):
            h.append(0) #on ajoute une case morte à h
        g_n.append(h) #on ajoute la liste h à la nouvelle grille
        
        #DERNIERE LIGNE DU CANON A PLANEURS
        h=[] #on vide la liste h pour obtenir les valeurs des cases de la nouvelle ligne
        for j in range(0, xg//2-6):
            h.append(0) #on ajoute une case morte à h
        for j in range(xg//2-6, xg//2-4):
            h.append(1)
        for j in range(xg//2-4, xg):
            h.append(0) #on ajoute une case morte à h
        g_n.append(h) #on ajoute la liste h à la nouvelle grille
        
        #LIGNES APRES LE CANON A PLANEURS
        for i in range(yg//2+5, yg): #de la première ligne à la dernière ligne où le planneur n'est pas généré (centré)
            h=[] #création de la liste h, représentant la ligne
            for j in range(0, xg): #pour chaque case de la ligne
                h.append(0) #on ajoute 0 pour chaque case blanche à la liste h
            g_n.append(h) #on ajoutre la liste h à la nouvelle grille
        g=deepcopy(g_n) #g prend la valeur de g_n avec prise en compte de l'imbriquement des listes
        
        #GENERATION DU CANON
        for i in range(0, yg): #pour chaque ligne de la grille
            for j in range(0, xg): #pour chaque case de chaque ligne
                if g[i][j] == 0: #si la valeur de la case est 0
                    can.create_rectangle((j*10),(i*10),(j*10)+10,(i*10)+10, fill='white') #alors on crée une case blanche
                else: #sinon
                    can.create_rectangle((j*10),(i*10),(j*10)+10,(i*10)+10, fill='black') #on crée une case noire
                    
def g_grand_vaisseau(): #fonction permettant de générer un grand vaisseau
    global g, xg, yg, tour, identique #g, xg, et yg en global car ne peuvent pas être pris comme argument dans la barre de menu
    identique = 0 #la nouvelle grille n'est pas ou plus identique à l'ancienne
    tour = 0 #le compteur de tours est réinitialisé
    cv = 13 #le compteur de cellule vivante est placé à 13 car il y a 13 cellules dans un grand vaisseau*
    text_compteur.config(text=tour) #le texte affichant le nombre de tours est actualisé
    if xg < 9 or yg < 9: #si la taille de la grille est insuffisante
        erreur6() #renvoie vers l'erreur 6
    else: #sinon, le grand vaisseau est généré
        text_cellulesvivantes2.config(text=cv) #le texte affichant le nombre de cellules vivantes est actualisé
        can.delete(ALL) #supprime les éléments du canevas
        g_n=[] #création d'une nouvelle grille
        
        #LIGNES AVANT LE VAISSEAU
        for i in range(0, yg//2-1): #pour toutes les lignes avant le vaisseau
            h=[] #on vide liste h pour obtenir les valeurs des cases de la nouvelle ligne
            for j in range(0, xg): #pour toutes les cases contenues dans ces lignes
                h.append(0) #on ajoute une case morte à h
            g_n.append(h) #on ajoute la liste h à la grille g_n
        
        #PREMIERE LIGNE DU VAISSEAU
        h=[] #on vide liste h pour obtenir les valeurs des cases de la nouvelle ligne
        for j in range(0, xg//2-2):
            h.append(0) #on ajoute une case morte à h
        for j in range(xg//2-2, xg//2+4):
            h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2+4, xg):
            h.append(0) #on ajoute une case morte à h
        g_n.append(h) #on ajoute la liste h à la grille g_n
        
        #DEUXIEME LIGNE DU VAISSEAU
        h=[] #on vide liste h pour obtenir les valeurs des cases de la nouvelle ligne
        for j in range(0, xg//2-3):
            h.append(0) #on ajoute une case morte à h
        h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2-2, xg//2+3):
            h.append(0) #on ajoute une case morte à h
        h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2+4, xg):
            h.append(0) #on ajoute une case morte à h
        g_n.append(h) #on ajoute la liste h à la grille g_n
        
        #TROISIEME LIGNE DU VAISSEAU
        h=[] #on vide liste h pour obtenir les valeurs des cases de la nouvelle ligne
        for j in range(0, xg//2+3):
            h.append(0) #on ajoute une case morte à h
        h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2+4, xg):
            h.append(0) #on ajoute une case morte à h
        g_n.append(h) #on ajoute la liste h à la grille g_n
        
        #QUATRIEME LIGNE DU VAISSEAU
        h=[] #on vide liste h pour obtenir les valeurs des cases de la nouvelle ligne
        for j in range(0, xg//2-3):
            h.append(0) #on ajoute une case morte à h
        h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2-2, xg//2+2):
            h.append(0) #on ajoute une case morte à h
        h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2+3, xg):
            h.append(0) #on ajoute une case morte à h
        g_n.append(h) #on ajoute la liste h à la grille g_n
        
        #CINQUIEME LIGNE DU VAISSEAU
        h=[] #on vide liste h pour obtenir les valeurs des cases de la nouvelle ligne
        for j in range(0, xg//2-1):
            h.append(0) #on ajoute une case morte à h
        for j in range(xg//2-1, xg//2+1):
            h.append(1) #on ajoute une case vivante à h
        for j in range(xg//2+1, xg):
            h.append(0) #on ajoute une case morte à h
        g_n.append(h) #on ajoute la liste h à la grille g_n
        
        #LIGNES APRES LE VAISSEAU
        for i in range(yg//2+4, yg): #pour toutes les lignes après le vaisseau
            h=[] #on vide liste h pour obtenir les valeurs des cases de la nouvelle ligne
            for j in range(0, xg): #pour toutes les cases contenues dans ces lignes
                h.append(0) #on ajoute une case morte à h
            g_n.append(h) #on ajoute la liste h à la grille g_n
        g=deepcopy(g_n) #g prend la valeur de g_n, avec prise en compte de la hiérarchie des listes dans g_n
        
        #GENERATION DU VAISSEAU
        for i in range(0, yg): #pour chaque ligne de la grille
            for j in range(0, xg): #pour chaque case de chaque ligne
                if g[i][j] == 0: #si la valeur de la case est 0
                    can.create_rectangle((j*10),(i*10),(j*10)+10,(i*10)+10, fill='white') #alors on crée une case blanche
                else: #sinon
                    can.create_rectangle((j*10),(i*10),(j*10)+10,(i*10)+10, fill='black') #on crée une case noire
        
#FONCTIONS DE CONSTRUCTION
                      
def g_construction(): #permet de construire la grille.
    global xg, yg, tour, identique #largeur et hauteur de la grille en cases.
    identique = 0 #la nouvelle grille n'est pas ou plus identique à l'ancienne
    tour = 0 #le compteur de tour est remis à 0
    text_compteur.config(text=tour) #le texte affichant le compteur de tour est actualisé
    can.delete(ALL) #supprime tous les éléments du canevas
    can.config(height=yg*10, width=xg*10) #configure les dimensions du canevas
    g_vide() #g vide est appelée pour créer la grille à l'intérieur du canvenas vide.

def g_changer_dimension():
    global xg, yg, dim, tour, identique
    tour = 0 #Réinitialise le compteur de tours à 0
    text_compteur.config(text=tour) #Permet d'afficher dans l'interface la nouvelle valeur du compteur
    xg_n = 0 #création d'une nouvelle variable pour la largeur
    yg_n = 0 #création d'une nouvelle variable pour la hauteur
    xg_n = int(elargeur.get()) #permet d'obtenir la valeur de la largeur entrée dans le champ de saisie
    yg_n = int(ehauteur.get()) #permet d'obtenir la valeur de la hauteur entrée dans le champ de saisie
    if xg_n < 3 and yg_n > 70: #si la largeur est inférieure à 3 et que la longueur est supérieure à 70
        erreur1() #renvoie à l'erreur 1
    elif xg_n > 70 and yg_n < 3: #si la hauteur est inférieure à 70 et que la largeur est supérieure à 70
        erreur1() #renvoie à l'erreur 1
    elif xg_n < 3 or yg_n < 3: #si la largeur et la hauteur sont inférieures à 3
        erreur2() #renvoie à l'erreur 2
    elif xg_n > 70 or yg_n > 70: #si la largeur et la hauteur sont supérieures à 70
        erreur3() #renvoie à l'erreur 3
    else: #sinon, les dimensions de la grilles sont changées
        identique=0 #la grille n'est plus identique à la précédente si elle l'était
        xg = xg_n #une nouvelle largeur est attribuée à la grille
        yg = yg_n #une nouvelle hauteur est attribuée à la grille
        g_construction() #appelle la fonction construisant la grille
        dim.destroy() #la fenêtre "dim" est détruite

#FONCTIONS D'ERREUR

def erreur1():
    erreur1=Tk()
    erreur1.title("Erreur 1")
    text_erreur1=Label(erreur1, text="Les valeurs doivent être comprises entre 3 et 70.")
    text_erreur1.pack()
    bouton_erreur1=Button(erreur1, text="OK", command=erreur1.destroy)
    bouton_erreur1.pack() 

def erreur2():
    erreur2=Tk()
    erreur2.title("Erreur 2")
    text_erreur2=Label(erreur2, text="La largeur et la hauteur doivent être supérieures ou égales à 3.")
    text_erreur2.pack()
    bouton_erreur2=Button(erreur2, text="OK", command=erreur2.destroy)
    bouton_erreur2.pack()

def erreur3():
    erreur3=Tk()
    erreur3.title("Erreur 3")
    text_erreur3=Label(erreur3, text="Les valeurs au dessus de 70 sont interdites car elles nuisent à la fluidité du programme.")
    text_erreur3.pack()
    bouton_erreur3=Button(erreur3, text="OK", command=erreur3.destroy)
    bouton_erreur3.pack()
    
def erreur4():
    erreur4=Tk()
    erreur4.title("Erreur 4")
    text_erreur4=Label(erreur4, text="La taille de la grille est insuffisante, elle doit contenir au moins 5x5 cases.")
    text_erreur4.pack()
    bouton_erreur4=Button(erreur4, text="OK", command=erreur4.destroy)
    bouton_erreur4.pack()

def erreur5():
    erreur5=Tk()
    erreur5.title("Erreur 5")
    text_erreur5=Label(erreur5, text="La largeur ne doit pas être inférieure à 18 et la hauteur ne doit pas être inférieure à 11.")
    text_erreur5.pack()
    bouton_erreur5=Button(erreur5, text="OK", command=erreur5.destroy)
    bouton_erreur5.pack()
    
def erreur6():
    erreur6=Tk()
    erreur6.title("Erreur 6")
    text_erreur6=Label(erreur6, text="La grille doit au moins contenir 9x9 cases.")
    text_erreur6.pack()
    bouton_erreur6=Button(erreur6, text="OK", command=erreur6.destroy)
    bouton_erreur6.pack()
    
def erreur7():
    erreur7=Tk()
    erreur7.title("Erreur 7")
    text_erreur7=Label(erreur7, text="La largeur ne doit pas être inférieure à 38 et la hauteur ne doit pas être inférieure à 11.")
    text_erreur7.pack()
    bouton_erreur7=Button(erreur7, text="OK", command=erreur7.destroy)
    bouton_erreur7.pack()
    
def erreur8():
    erreur8=Tk()
    erreur8.title("Erreur 8")
    text_erreur8=Label(erreur8, text="Le nombre de tours ne doit pas être négatif.")
    text_erreur8.pack()
    bouton_erreur8=Button(erreur8, text="OK", command=erreur8.destroy)
    bouton_erreur8.pack()

#FONCTIONS DE PARAMETRAGE

def g_dimension(): #fonction créant une fenêtre pour changer les dimensions de la grille
    global xg, yg, elargeur, ehauteur, dim, identique
    dim=Tk() #crée une fenêtre pour les dimensions
    dim.title("Dimensions de la grille") #titre de la fenêtre
    texte=Label(dim, text="Dimensions de la grille") #texte
    texte.pack() #permet d'afficher le texte
    texte0=Label(dim, text="Nombres de cases") #texte
    texte0.pack() #permet d'afficher le texte
    texte1=Label(dim, text="Largeur") #texte
    texte1.pack() #permet d'afficher le texte
    elargeur = Spinbox(dim, from_=3, to=70) #permet d'entrée une nouvelle valeur pour la largeur
    elargeur.pack() #permet d'afficher le champ de saisie
    texte2=Label(dim, text="Hauteur") #texte
    texte2.pack() #permet d'afficher le texte
    ehauteur = Spinbox(dim, from_=3, to=70) #permet d'entrer une nouvelle valeur pour la hauteur
    ehauteur.pack() #permet d'afficher le champ de saisie
    annuler = Button(dim, text="Annuler", command=dim.destroy) #bouton permettant de quitter la fenêtre
    annuler.pack() #permet d'afficher le bouton
    ok = Button(dim, text="Appliquer", command=g_changer_dimension) #bouton permettant de changer les dimensions
    ok.pack() #permet d'afficher le bouton

def g_bordure(): #fonction permettant de créer une fenêtre pour activer ou désictiver les bordures
    global bordure, bord #utilisation de la variable globale bordure et de la fenêtre bord
    bord=Tk() #crée une fenêtre pour les bordures
    bord.title("Bordures de la grille") #titre de la fenêtre
    activerbordure=Button(bord, text="Activer les bordures de la grille", command=g_bordure2)
    activerbordure.pack()
    desactiverbordure = Button(bord, text="Désactiver les bordures de la grille", command=g_bordure3)
    desactiverbordure.pack()
    
def g_bordure2(): #fonction activant les bordures
    global bordure, bord #utilisation de la variable globale bordure et de la fenêtre bord
    bordure = 1 #la variable bordure prend la 1 pour activer les bordures
    bord.destroy() #supprime la fenêtre "bord"
    
def g_bordure3(): #fontion désactivant les bordures
    global bordure, bord #utilisation de la variable globale bordure et de la fenêtre bord
    bordure = 0 #la variable bordure prend la 1 pour désactiver les bordures
    bord.destroy() #supprime la fenêtre "bord"

def g_tour(): #fonction créant une fenêtre pour changer le nombre maximal de tours
    global tour_fin, etour, fentour #variables globales, tkinter oblige
    fentour=Tk() #création d'une fenêtre pour modifier le nombre de tours
    fentour.title("Tours") #Titre de la fenêtre
    texte10=Label(fentour, text="Nombre de tours maximum de génération") #Affiche du texte dans la fenêtre
    texte10.pack() #Permet d'afficher le label dans la fenêtre
    etour = Spinbox(fentour, from_=0, to=10000) #Permet de déterminer un nombre de tour maximum grâce à un champ de saisie
    etour.pack() #permet d'afficher le champ de saisie dans la fenêtre
    texte11=Label(fentour, text="Mettre à 0 pour qu'il n'y ait pas de limite") #Affiche du texte dans la fenêtre
    texte11.pack() #permet de le rendre visible dans l'interface
    annuler5 = Button(fentour, text="Annuler", command=fentour.destroy) #bouton permettant de quitter la fenêtre
    annuler5.pack() #permet d'afficher le bouton
    ok = Button(fentour, text="Appliquer", command=g_changer_tour) #bouton permettant de changer les dimensions
    ok.pack() #permet d'afficher le bouton
    
def g_changer_tour(): #fonction permettant de changer le nombre de tour maximal
    global tour_fin, etour, fentour #prend on compte les variables globales: tour maximal, tour maximal entré et la fenêtre des tours
    tour_fin_n=0 #création d'un nouvelle variable de tour maximal
    tour_fin_n = int(etour.get()) #la variable prend la valeur entrée par l'utilisateur dans la champ de saisie
    if tour_fin_n < 0: #si le nombre de tour entré est négatif
        erreur8() #renvoie vers l'erreur 8
    else: #sinon
        tour_fin = tour_fin_n #le nombre de tour maximal est modifié
        text_fincompteur.config(text=tour_fin) #le texte affichant le nombre de tour maximal est actualisé
        fentour.destroy() #la fenêtre est détruite

#FONCTIONS DE SIMULATION
        
def g_simulation_1_etape(): #fonction permettant de simuler étape par étape
    global tour, identique, tour_fin, bordure #variables globales
    if identique == 0 and (tour != tour_fin or tour_fin == 0): #si les grilles ne sont pas identiques et qu'il n'y a pas de limite de tour ou que le tour maximal n'est pas arrivé
        if bordure == 1: #si les bordures sont activées
            g_simulation_bordures() #simulation avec bordures
        else: #sinon
            g_simulation_sans_bordures() #simulation sans bordures
        tour = tour + 1 #le compteur de tours est incrémenté de 1
        text_compteur.config(text=tour) #le texte affichant le compteur de tour est actualisé
    
def g_delais(): #fonction permettant de simuler en continu la grille
    global tour, tours_au_debut, debut
    debut = time.time() #debut prend la valeur du temps actuel
    tours_au_debut= tour #le nombre de tours au début est égal au nombre de tours passés
    g_boucle() #appeler la fonction permettant de simuler en boucle
    
def g_boucle(): #fonction permettant de simuler en boucle
    global tour, identique, tour_fin, bordure, tours_au_debut, debut, fait
    fait=0 #la simulation n'a pas été effectuée
    while identique == 0 and (tour != tour_fin or tour_fin == 0) and tour-tours_au_debut < 50:
        if fait==0: #si la simulation n'a pas été effectuée
            if bordure ==1: #si les bordures sont activées
                g_simulation_bordures() #appelle la fonction de génération qui prend en compte les bordures
            else: #sinon, si elles sont désactivées
                g_simulation_sans_bordures() #appelle la fonction de génération qui ne prend pas en compte les bordures
            fait=1 #la simulation a été effectuée
            tour = tour + 1 #on incrémente le nombre de tour
            text_compteur.config(text=tour) #le texte affichant le nombre de tours effectués est actualisé.
            print(tour) #pour montrer que la fonction est fonctionnelle et qu'il n'y a juste qu'un problème au niveau de l'interface graphique
        if time.time() - debut > 0.3*(tour - tours_au_debut+1): #si le temps d'attente pour le tour suivant est écoulé
            g_boucle() #on rappelle la fonction elle-même
                     
def g_simulation_sans_bordures(): #simulation de la grille sans bordures
    global g, xg, yg, identique, cv #ces variables sont globales, car les widjets tkinter ne prennent pas d'argument dans les fonctions
    v=0 #compteur de cellules vivantes voisines à 0
    cv=0 #compteir de cellules vivantes totales à 0
    g_n=[] #création d'une nouvelle grille
    for i in range(0,yg): #pour chaque ligne
        h=[] #création d'un liste h
        if i != yg-1: #pour la dernière ligne de la grille
            for j in range(0, xg): #pour chaque case de la ligne
                if j != xg-1: #pour toutes les cases de la ligne sauf la dernière
                    if g[i-1][j-1] == 1: #on regarde si la case située en haut à gauche
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i-1][j-0] == 1: #on regarde si la case située au dessus
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i-1][j+1] == 1: #on regarde si la case située en haut à droite
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i][j-1] == 1: #on regarde si la case située à sa gauche
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i][j+1] == 1: #on regarde si la case située à sa droite
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i+1][j-1] == 1: #on regarde si la case située en bas à gauche
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i+1][j] == 1: #on regarde si la case située en dessous
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i+1][j+1] == 1: #on regarde si la case située en bas à droite
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                else: #pour la dernière case de la ligne
                    if g[i-1][j-1] == 1: #on regarde si la case située en haut à gauche
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i-1][j-0] == 1: #on regarde si la case située au dessus
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i-1][0] == 1: #on regarde si la case située en haut à droite
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i][j-1] == 1: #on regarde si la case située à sa gauche
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i][0] == 1: #on regarde si la case située à sa droite
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i+1][j-1] == 1: #on regarde si la case située en bas à gauche
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i+1][j] == 1: #on regarde si la case située en dessous
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i+1][0] == 1: #on regarde si la case située en bas à droite
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                if g[i][j]== 1: #si la case était vivante à la grille d'avant
                    if v == 2 or v == 3: #si il y avait 2 ou 3 cellules vivantes autour
                        h.append(1) #la cellule prend la valeur 1 et est ajouté à la liste de sa ligne
                        cv= cv + 1 #le nombre de cellules vivante de la grille est incrémenté de 1
                    else: #sinon
                        h.append(0) #la cellule prend la valeur 0 et est ajoutée à la liste de sa ligne
                else: #sinon la case était morte à la grille d'avant
                    if v == 3: #s'il y avait 3 cellules vivantes autour
                        h.append(1) #la cellule prend la valeur 1 et est ajoutée à la liste de sa ligne
                        cv= cv + 1 #le nombre de cellules vivante de la grille est incrémenté de 1
                    else: #sinon
                        h.append(0) #la cellule prend la valeur 0 et est ajoutée à la liste de sa ligne
                v=0 #le compteur de cellules vivantes est réinitialisé après l'attribution d'une valeur à une case
        else:
            for j in range(0, xg): #pour chaque case de la ligne
                if j != xg-1:
                    if g[i-1][j-1] == 1: #on regarde si la case située en haut à gauche
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i-1][j-0] == 1: #on regarde si la case située au dessus
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i-1][j+1] == 1: #on regarde si la case située en haut à droite
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i][j-1] == 1: #on regarde si la case située à sa gauche
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i][j+1] == 1: #on regarde si la case située à sa droite
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[0][j-1] == 1: #on regarde si la case située en bas à gauche
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[0][j] == 1: #on regarde si la case située en dessous
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[0][j+1] == 1: #on regarde si la case située en bas à droite
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                else:
                    if g[i-1][j-1] == 1: #on regarde si la case située en haut à gauche
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i-1][j-0] == 1: #on regarde si la case située au dessus
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i-1][0] == 1: #on regarde si la case située en haut à droite
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i][j-1] == 1: #on regarde si la case située à sa gauche
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i][0] == 1: #on regarde si la case située à sa droite
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[0][j-1] == 1: #on regarde si la case située en bas à gauche
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[0][j] == 1: #on regarde si la case située en dessous
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[0][0] == 1: #on regarde si la case située en bas à droite
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                if g[i][j]== 1: #si la case était vivante à la grille d'avant
                    if v == 2 or v == 3: #si il y avait 2 ou 3 cellules vivantes autour
                        h.append(1) #la cellule prend la valeur 1 et est ajouté à la liste de sa ligne
                        cv= cv + 1 #le nombre de cellules vivante de la grille est incrémenté de 1
                    else: #sinon
                        h.append(0) #la cellule prend la valeur 0 et est ajoutée à la liste de sa ligne
                else: #sinon la case était morte à la grille d'avant
                    if v == 3: #s'il y avait 3 cellules vivantes autour
                        h.append(1) #la cellule prend la valeur 1 et est ajoutée à la liste de sa ligne
                        cv= cv + 1 #le nombre de cellules vivante de la grille est incrémenté de 1
                    else: #sinon
                        h.append(0) #la cellule prend la valeur 0 et est ajoutée à la liste de sa ligne
                v=0 #le compteur de cellules vivantes est réinitialisé après l'attribution d'une valeur à une case
        g_n.append(h) #on ajoute la liste de la ligne à la nouvelle grille
    if g == g_n: #si la grille précédente et la nouvelle sont identiques
        identique = 1 #alors la variable identique prend la valeur 1
    else: #sinon
        identique = 0 #elle reste à 0
    g=deepcopy(g_n) #g prend la valeur de g_n avec prise en compte de la hiérarchie des listes dans g_n
    can.delete(ALL) #suppression de tous les éléments de canevas
    for i in range(0, yg): #pour chaque ligne
        for j in range(0, xg): #pour chaque case de la ligne
            if g[i][j] == 1: #si la case est noire
                can.create_rectangle((j*10),(i*10),(j*10)+10,(i*10)+10, fill='black') #crée une case noire sur le canevas.
            else: #si la case est blanche
                can.create_rectangle((j*10),(i*10),(j*10)+10,(i*10)+10, fill='white') #crée une case blanche sur le canevas.
    text_cellulesvivantes2.config(text=cv) #change le nombre de cellules vivantes affiché
    
def g_simulation_bordures(): #simulation de la grille avec bordures
    global g, xg, yg, identique, cv #globales car les fonctions appelées par les wigdets tkinter ne prennent pas d'arguments
    v=0 #nombre de cellules vivantes
    cv=0 #nombre de cellules vivantes à chaque tour
    g_n=[] #on créé une nouvelle grille, qui va remplacer l'ancienne
    for i in range(0, yg): # i variable qui parcourt la largeur de la grille
        if i==0: #si la cellule est sur la ligne du haut
            h=[] #on créé une nouvelle liste
            for j in range(0, xg): #pour chaque case de la ligne
                if j==0: #cas de la cellule dans l'angle d'en haut à gauche
                    if g[i][j+1] == 1: #on regarde si la case située à sa droite est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i+1][j] == 1: #on regarde si la case située en dessous est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i+1][j+1] == 1: #on regarde si la case située en bas à droite est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                elif j== xg-1: #cas de la cellule dans l'angle d'en haut à droite
                    if g[i][j-1]== 1: #on regarde si la case à sa gauche est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i+1][j] == 1: #on regarde si la case située en dessous est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i+1][j-1] == 1: #on regarde si la case située à sa gauche en dessous est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                else: #autres cellules de la ligne du haut
                    if g[i][j-1]== 1: #on regarde si la case à sa gauche est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i][j+1]== 1: #on regarde si la case à sa droite est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i+1][j-1] == 1: #on regarde si la case qui lui est en bas à gauche est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i+1][j] == 1: #on regarde si la case qui lui est en dessous est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i+1][j+1] == 1: #on regarde si la case qui lui est en bas à droite est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                if g[i][j]== 1: #si la case était vivante à la grille d'avant
                    if v == 2 or v == 3: #si il y avait 2 ou 3 cellules vivantes autour
                        h.append(1) #la cellule prend la valeur 1 et est ajouté à la liste de sa ligne
                        cv= cv + 1 #le nombre de cellules vivante de la grille est incrémenté de 1
                    else: #sinon
                        h.append(0) #la cellule prend la valeur 0 et est ajoutée à la liste de sa ligne
                else: #sinon la case était morte à la grille d'avant
                    if v == 3: #s'il y avait 3 cellules vivantes autour
                        h.append(1) #la cellule prend la valeur 1 et est ajoutée à la liste de sa ligne
                        cv= cv + 1 #le nombre de cellules vivante de la grille est incrémenté de 1
                    else: #sinon
                        h.append(0) #la cellule prend la valeur 0 et est ajoutée à la liste de sa ligne
                v=0 #le compteur de cellules vivantes est réinitialisé après l'attribution d'une valeur à une case
        elif i==yg-1: #sinon, si la cellule est sur la ligne du bas
            h=[] #on créé une nouvelle liste pour une ligne
            for j in range(0, xg): #pour chaque case de la ligne
                if j==0: #cas de la cellule dans l'angle d'en bas à gauche
                    if g[i][j+1] == 1: #on regarde si la case à sa droite est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i-1][j] == 1: #on regarde si la case qui lui est au dessus est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i-1][j+1] == 1: #si regarde si la case qui lui est en haut à droite est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                elif j== xg-1: #cas de la cellule dans l'angle d'en bas à droite
                    if g[i][j-1]== 1: #on regarde si la case à sa gauche est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i-1][j] == 1: #on regarde si la case du dessus est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i-1][j-1] == 1: #on regarde si la case qui lui est en haut à gauche est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                else: #autres cellules de la ligne du bas
                    if g[i][j-1]== 1: #on regarde si la case à sa gauche est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i][j+1]== 1: #on regarde si la case à sa droite est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i-1][j-1] == 1: #on regarde si la case qui lui est en haut à gauche est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i-1][j] == 1: #on regarde si la case du dessus est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i-1][j+1] == 1: #on regarde si la case qui lui est en haut à droite est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                if g[i][j]== 1: #si la case était vivante à la grille d'avant
                    if v == 2 or v == 3: #si il y avait 2 ou 3 cellules vivantes autour
                        h.append(1) #la cellule prend la valeur 1 et est ajouté à la liste de sa ligne
                        cv= cv + 1 #le nombre de cellules vivante de la grille est incrémenté de 1
                    else: #sinon
                        h.append(0) #la cellule prend la valeur 0 et est ajoutée à la liste de sa ligne
                else: #sinon la case était morte à la grille d'avant
                    if v == 3: #s'il y avait 3 cellules vivantes autour
                        h.append(1) #la cellule prend la valeur 1 et est ajouté à la liste de sa ligne
                        cv= cv + 1 #le nombre de cellules vivante de la grille est incrémenté de 1
                    else: #sinon
                        h.append(0) #la cellule prend la valeur 0 et est ajoutée à la liste de sa ligne
                v=0 #le compteur de cellules vivantes est réinitialisé après l'attribution d'une valeur à une case
        else: #Pour toutes les cellules situées sur les lignes intermédiaires
            h=[] #on créé une nouvelle liste pour une ligne
            for j in range(0, xg):
                if j==0: #cas d'une cellule sur la colonne de gauche
                    if g[i-1][j] == 1: #on regarde si la case du dessus est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i-1][j+1] == 1: #on regarde si la case qui lui est au dessus à droite est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i][j+1] == 1: #on regarde si la case à sa droite est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i+1][j] == 1: #on regarde si la case du dessous est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i+1][j+1] == 1: #on regarde si la case qui lui est en bas à droite est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                elif j== xg-1: #cas d'une cellule sur la colonne de droite
                    if g[i-1][j] == 1: #on regarde si la case du dessus est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i-1][j-1] == 1: #on regarde si la case qui lui est au dessus à gauche est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i][j-1] == 1: #on regarde si la case à sa gauche est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i+1][j] == 1: #on regarde si la case du dessous est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i+1][j-1] == 1: #on regarde si la case qui lui est en bas à gauche est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                else: #pour toutes les autres cellules
                    if g[i-1][j-1] == 1: #on regarde si la case qui lui est en haut à gauche est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i-1][j] == 1: #on regarde si la case du dessus est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i-1][j+1] == 1: #on regarde si la case qui lui est en haut à droite est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i][j-1] == 1: #on regarde si la case de gauche est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i][j+1] == 1: #on regarde si la case de droite est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i+1][j-1] == 1: #on regarde si la case qui lui est en bas à gauche est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i+1][j] == 1: #on regarde si la case du dessous est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                    if g[i+1][j+1] == 1: #on regarde si la case qui lui est en bas à droite est vivante
                        v = v+1 #si oui on ajoute 1 au compteur de cellules vivantes
                if g[i][j]== 1: #si la case était vivante à la grille d'avant
                    if v == 2 or v == 3: #si il y avait 2 ou 3 cellules vivantes autour
                        h.append(1) #la cellule prend la valeur 1 et est ajouté à la liste de sa ligne
                        cv= cv + 1 #le nombre de cellules vivante de la grille est incrémenté de 1
                    else: #sinon
                        h.append(0) #la cellule prend la valeur 0 et est ajoutée à la liste de sa ligne
                else: #sinon la case était morte à la grille d'avant
                    if v == 3: #s'il y avait 3 cellules vivantes autour
                        h.append(1) #la cellule prend la valeur 1 et est ajouté à la liste de sa ligne
                        cv= cv + 1 #le nombre de cellules vivante de la grille est incrémenté de 1
                    else: #sinon
                        h.append(0) #la cellule prend la valeur 0 et est ajoutée à la liste de sa ligne
                v=0 #le compteur de cellules vivantes est réinitialisé après l'attribution d'une valeur à une case
        g_n.append(h) #on ajoute la liste de la ligne à la nouvelle grille
    if g == g_n: #si la grille précédente est identique à la nouvelle
        identique = 1 #la variable identique prend la valeur 1
    else: #sinon
        identique = 0 #elle reste à 0
    g=deepcopy(g_n) #g prend la valeur de g_n avec prise en compte de la hiérarchie des listes dans g_n
    can.delete(ALL) #suppression de tous les éléments du canevas
    for i in range(0, yg): #pour chaque ligne
        for j in range(0, xg): #pour chaque case de la ligne
            if g[i][j] == 1: #si la case est noire
                can.create_rectangle((j*10),(i*10),(j*10)+10,(i*10)+10, fill='black') #crée une case noire sur le canevas.
            else: #si la case est blanche
                can.create_rectangle((j*10),(i*10),(j*10)+10,(i*10)+10, fill='white') #crée une case blanche sur le canevas.
    text_cellulesvivantes2.config(text=cv) #change le nombre de cellules vivantes affiché

def clic(event): #fonction permettant de changer la valeur d'une case de la grille si elle est cliqué
    global identique, g, tour, cv #variable identique, compteur de tour, comtpeur de cellules vivantes et grille du canevas sont globales
    identique = 0 #les grilles ne sont plus identiques
    tour = 0 #le compteur de tour est réinitialisé
    text_compteur.config(text=tour) #le texte affichant le nombre de tours effectués est actualisé.
    xclic = 0 #création d'une variable de largeur pour la case correspondant à l'endroit cliqué
    yclic = 0 #création d'une variable de hauteur pour la case correspondant à l'endroit cliqué
    xclic = event.x//10 #permet d'obtenir l'emplacement dans la liste g de la case cliquée selon x
    yclic = event.y//10 #permet d'obtenir l'emplacement dans la liste g de la case cliquée selon y
    if g[yclic][xclic] == 1: #si la case était noire
        g[yclic][xclic] = 0 #alors elle deviant blanche
        cv = cv - 1 #on perd une cellule vivante dans le compteur
        text_cellulesvivantes2.config(text=cv) #permet d'actualiser la valeur de compteur de cellules vivantes affiché
        can.create_rectangle((xclic*10),(yclic*10),(xclic*10)+10,(yclic*10)+10, fill='white')#crée une case blanche sur le canevas.
    else: #sinon
        g[yclic][xclic] = 1 #elle deviant noire
        cv = cv + 1 #on gagne une cellule vivante dans le compteur
        text_cellulesvivantes2.config(text=cv) #permet d'actualiser la valeur de compteur de cellules vivantes affiché
        can.create_rectangle((xclic*10),(yclic*10),(xclic*10)+10,(yclic*10)+10, fill='black') #crée une case noire sur le canevas.

#Création de la fenêtre principale
fen=Tk() #création de la fenêtre principale
fen.title("PROJET ISN - Le Jeu de la Vie, de Tony TAVERNIER, Hugo RONEZ et Titouan RAMAGE") #titre de la fenêtre

#Création du canevas à l'intérieur de la fenêtre principale
can=Canvas(fen,bg='#FFFFFF', height = yg*10, width = xg*10) #création de la grille dans la fenêtre de l'interface graphique.
can.pack() #permet d'afficher le canevas dans la fenêtre
can.bind("<Button-1>", clic) #permet d'appeller la fonction clic lorsqu'il y a un clic gauche sur le canevas

#Création du texte à l'intérieur de la fenêtre principale
text_compteur=Label(fen, text=tour) #permet de créer le texte affichant le nombre de tours effectués
text_compteur.pack() #permet d'afficher ce texte dans la fenêtre
text_sur=Label(fen, text=" / ") #création du texte "/"
text_sur.pack() #Affiche "/"
text_fincompteur=Label(fen, text=tour_fin) #permet de créer le texte affichant le nombre de tours maximal
text_fincompteur.pack() #permet d'afficher ce nombre de tours dans la fenêtre
text_cellulesvivantes=Label(fen, text="Nombres de cellules vivantes:")
text_cellulesvivantes.pack()
text_cellulesvivantes2=Label(fen, text=cv) #texte récupérant le nombre de cellule vivante dans la grille
text_cellulesvivantes2.pack() #permet d'afficher ce texte dans la fenêtre

g_construction() #appelle la fonction permettant de construire la grille

#Création des boutons de la fenêtre principale
simuler = Button(fen, text="Simuler étape par étape", command=g_simulation_1_etape) #bouton permettant de simuler étape par étape
simuler.pack() #permet d'afficher ce bouton
simulertout = Button(fen, text="Simuler en continu", command=g_delais) #bouton permettant de simuler en continu
simulertout.pack() #permet d'afficher ce bouton

#Création du menu de la fenêtre principale
menubar = Menu(fen) #création d'un menu
#Menu pour quitter
menu_quit = Menu(menubar, tearoff=0) #création du menu pour quitter, dans le menu principal
menu_quit.add_command(label="Quitter", command=fen.destroy) #permet de quitter le logiciel
menubar.add_cascade(label="Quitter", menu=menu_quit)
#Menu des options de la grille
menu_option = Menu(menubar, tearoff=0) #création du menu pour les options dans le menu principal
menu_option.add_command(label="Dimensions", command=g_dimension) #permet de changer les dimensions de la grille.
menu_option.add_command(label="Bordures", command=g_bordure) #permet d'activer ou de désactiver les bordures.
menu_option.add_command(label="Tours", command=g_tour) #permet de changer le nombre de tours maximal
menubar.add_cascade(label="Options", menu=menu_option)
#Menu pour générer des structures
menu_gen = Menu(menubar, tearoff=0) #création du menu pour la génération dans le menu principal
menu_gen.add_command(label="Vider la grille", command=g_vide) #permet de vider la grille
menu_gen.add_command(label="Aléatoire", command=g_alea) #permet de générer aléatoirement une grille
menu_gen.add_command(label="Aléatoire avec symétrie verticale", command=g_alea_sym_v) #permet de générer aléaoirement une grille avec symétrie verticale
menu_gen.add_command(label="Aléatoire avec symétrie horizontale", command=g_alea_sym_h) #permet de générer aléatoirement une grille avec symétrie horizontale
menu_gen.add_command(label="Planeur", command=g_planeur) #permet de générer un planeur.
menu_gen.add_command(label="Grand Vaisseau", command=g_grand_vaisseau) #permet de générer un grand vaisseau.
menu_gen.add_command(label="Canon à planeurs", command=g_canon_a_planeurs) #permet de générer un canon à planeur.
menu_gen.add_command(label="Clignotant", command=g_clignotant) #permet de générer un clignotant.
menu_gen.add_command(label="Pentadécathlon", command=g_pentadecathlon) #permet de générer un pentadécathlon.
menubar.add_cascade(label="Générer", menu=menu_gen)

fen.config(menu=menubar) #actualise le menu de la fenêtre en devenant similaire au menu créé au dessus

fen.mainloop() #permet de faire fonctionner la fenêtre principale en continu

